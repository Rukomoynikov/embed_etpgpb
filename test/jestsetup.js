import { shallow, render, mount } from 'enzyme';
import { h } from 'preact';

global.shallow = shallow
global.render = render
global.mount = mount
global.h = h

global.API_URL = "http://localhost:3004/db"

      // : ENV === 'development' ?
      //     JSON.stringify() :
      //     JSON.stringify("http://51.15.42.188/api/procedures?url=")

console.error = message => {
  throw new Error(message)
}