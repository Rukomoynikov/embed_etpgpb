import { h } from 'preact';
import {shallow} from 'preact-render-spy';

import Main from '../src/components/Main'

it('should render a label', () => {
    const wrapper = shallow(
        <Main mode='slim'/>
    );
    expect(wrapper).toMatchSnapshot();
});