const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ENV = process.env.NODE_ENV || 'development';

const CSS_MAPS = ENV!=='production';

module.exports = {
  // entry: './src/index.jsx',
  entry: {
    procedures: './src/index.jsx',
    embed: './src/components/EmbedCodeGenrator.jsx',
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: "[name].js"
  },
  // output: {
  //   filename: 'bundle.js',
  //   path: path.resolve(__dirname, 'public')
  // },
  resolve: {
    "alias": {
      "react": "preact-compat",
      "react-dom": "preact-compat",
      "components": path.resolve(__dirname, 'src/components'),
      "css": path.resolve(__dirname, 'src/css'),
      "svg": path.resolve(__dirname, 'src/svg')
    }
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: path.resolve(__dirname, 'src'),
        enforce: 'pre',
        use: 'source-map-loader'
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.scss$/,
        use: [{
            loader: "style-loader" // creates style nodes from JS strings
        }, {
            loader: "css-loader" // translates CSS into CommonJS
        }, {
            loader: "sass-loader" // compiles Sass to CSS
        }]
      },
      {
        test: /\.css$/,
        use: [{
            loader: "style-loader" // creates style nodes from JS strings
        }, {
            loader: "css-loader" // translates CSS into CommonJS
        }]
      },
      {
        test: /\.json$/,
        use: 'json-loader'
      },
      {
        test: /\.(xml|html|txt|md)$/,
        use: 'raw-loader'
      },
      {
        test: /\.(svg|woff2?|ttf|eot|jpe?g|png|gif)(\?.*)?$/i,
        use: ENV==='production' ? 'file-loader' : 'url-loader'
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      API_URL: ENV === 'development' ?
          JSON.stringify('http://localhost:3004/db') :
          JSON.stringify("http://51.15.78.159/api/procedures?url=")
    })
  ]
};