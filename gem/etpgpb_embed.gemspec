# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'etpgpb_embed/version'

Gem::Specification.new do |spec|
  spec.name          = "etpgpb_embed"
  spec.version       = EtpgpbEmbed::VERSION
  spec.authors       = ["Maksim Rukomoynikov"]
  spec.email         = ["m.rukomoynikov@etpgpb.ru"]

  spec.summary       = %q{Интеграция процедур с ЭТП ГПБ на свой сайт}
  spec.homepage      = "https://etpgpb.ru"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.

  spec.files         = ['app/assets/javascripts/procedures.js']

  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.14"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
