import ProceduresList from 'components/ProceduresList.jsx';

require('css/SlimProceduresList.scss');

export default class SlimProceduresList extends ProceduresList {
  constructor(props) {
    super(props);
  }

  renderProcedure (procedure) {
    return (
      <a className="procedure procedure--slim" href={"https://etpgpb.ru" + procedure.url} target="_blank">

        <div className="procedure__companyInfo">
          <div className="block__related_about_number">{ procedure.section }</div>
          <div> { procedure.company } </div>
          <div>{ procedure.procedure_status }</div>
        </div>

        <div className="procedure__lotInfo" itemscope="" itemtype="http://schema.org/Event">
          <div className="procedure__title" itemprop="name">{ procedure.title }</div>
          <div className="block__related_info_text" itemprop="description">{ procedure.description }</div>
          <div className="procedure__links">
            <span className="block__related_info_links_link">{ procedure.ep_status }</span>
            <span className="block__related_info_links_link">{ procedure.security_payment_status }</span>
          </div>
        </div>

        <div className="procedure__priceInfo">

          <div className="procedurePrice">
            <span className='procedurePrice__num'> { procedure.price }</span>
            { procedure.currency === 'rouble' &&
            <span className="block__related_details_currency">
                <svg id="icon-rouble" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 23 32">
                  <g fill="currentColor">
                    <path d="M18.625 10.089q0-1.786-1.161-2.893t-3.054-1.107h-5.714v8h5.714q1.893 0 3.054-1.107t1.161-2.893zM22.857 10.089q0 3.446-2.259 5.625t-5.83 2.179h-6.071v2.107h9.018q0.25 0 0.411 0.161t0.161 0.411v2.286q0 0.25-0.161 0.411t-0.411 0.161h-9.018v3.429q0 0.25-0.17 0.411t-0.402 0.161h-2.982q-0.25 0-0.411-0.161t-0.161-0.411v-3.429h-4q-0.25 0-0.411-0.161t-0.161-0.411v-2.286q0-0.25 0.161-0.411t0.411-0.161h4v-2.107h-4q-0.25 0-0.411-0.161t-0.161-0.411v-2.661q0-0.232 0.161-0.402t0.411-0.17h4v-11.232q0-0.25 0.161-0.411t0.411-0.161h9.625q3.571 0 5.83 2.179t2.259 5.625z"/>
                  </g>
                </svg>
              </span>
            }
          </div>

          <div className="procedure__acceptingDate">
            Прием заявок
            <br />
            <span>{ procedure.accept_date }</span>
          </div>
        </div>

      </a>
    );
  }
}