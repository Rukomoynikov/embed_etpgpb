import { h, render, Component } from 'preact';
import Highlight from 'react-hljs'

require('css/EmbedCodeGenerator.scss');

class EmbedCodeGenerator extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='code'>
        <div className='code__text'>

        </div>
      </div>
    )
  }
  
}

document.addEventListener('DOMContentLoaded', function() {
  const container = document.querySelector('#code');

  render(<EmbedCodeGenerator />, container);
});