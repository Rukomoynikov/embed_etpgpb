import { h, render, Component } from 'preact';

import SlimProceduresList from 'components/SlimProceduresList.jsx';
import WideProceduresList from 'components/WideProceduresList.jsx';

export default class Main extends Component {

  constructor(props) {
    super(props);
  }

  render() {
  	if (this.props.mode == 'slim') {
	    return <div><SlimProceduresList {...this.props} /></div>
  	} else {
	    return <div><WideProceduresList {...this.props} /></div>
  	}
  }
  
}