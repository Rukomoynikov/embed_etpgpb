// @flow
import { h, Component } from 'preact';
import axios from 'axios';
import Spinner from 'react-spinkit';

require('../main.scss');
require('css/pagination.scss');

const URL = API_URL;

export default class ProceduresList extends Component {
  
  /**
   * @param props          Information about the object.
   * @param props.limit   Information about the object's members.
   */
  
  constructor(props) {
    super(props);
    
    this.state = {
      limit: props.limit || null,
      procedures: [],
      pagination: {},
      loading: true,
      loadingPagination: false,
      url: URL + props.url
    };
  }
  
  render() {
    if (this.state.loading) {
      return (
        <div className='loading'>
          <Spinner name='line-scale' />
        </div>
      )
    } else {
      return (
        <div className='procedures'>
          { this.state.procedures.map(procedure => this.renderProcedure (procedure)) }
          { this.renderPagination() }
        </div>
      )
    }
  }
  
  componentDidMount() {
    let {url } = this.state;
    
    axios.get(url)
      .then( (response) => {
        let procedures = response.data.lots;
        let pagination = response.data.pagination;
        
        if (this.props.limit > 0) {
          pagination = false;
          procedures = procedures.slice(0, this.props.limit)
        }
        
        this.setState({
          procedures,
          pagination,
          loading: false
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  
  renderProcedure(procedure) {
    return (
      <a className="procedure" href={"https://etpgpb.ru" + procedure.url} target="_blank">
        
        <div className="procedure__companyInfo">
          <div className="block__related_about_number">{ procedure.section }</div>
          <div className="block__related_about_company">
            <img src={ procedure.company_logo_url } alt={ procedure.company }/>
          </div>
          <div>{ procedure.procedure_status }</div>
        </div>
        
        <div className="procedure__lotInfo" itemscope="" itemtype="http://schema.org/Event">
          <div className="procedure__title" itemprop="name">{ procedure.title }</div>
          <div className="block__related_info_text" itemprop="description">{ procedure.description }</div>
          <div className="procedure__links">
            <span className="block__related_info_links_link">{ procedure.ep_status }</span>
            <span className="block__related_info_links_link">{ procedure.security_payment_status }</span>
          </div>
        </div>
        
        <div className="procedure__priceInfo">
          
          <div className="procedurePrice">
            <span className='procedurePrice__num'>{ procedure.price }</span>
            <span className="block__related_details_currency">
              { this.renderCurrency(procedure.currency) }
            </span>
          </div>
          
          <div className="procedure__acceptingDate">
            Прием заявок
            <br />
            <span>{ procedure.accept_date }</span>
          </div>
        </div>
      
      </a>
    )
  }

  renderCurrency (currencyName) {
    let currencyIMG = require(`svg/rouble.svg`)

    return (
      <img src={ currencyIMG } />
    )
  }

  renderPagination() {
    if (this.state.nextPage === null) {
      return null;
    }
    
    if (this.state.loadingPagination) {
      return (
        <div className='pagination'>
          <Spinner name='line-scale'/>
        </div>
      )
    } else {
      return (
        <div className='pagination'>
          <button
            className="pagination__button"
            onClick={this.getMoreProcedures.bind(this)}> Ещё
          </button>
        </div>
      )
    }
  }
  
  getMoreProcedures() {
    let nextPage = this.state.pagination.nextPage;
    
    this.setState({
      loadingPagination: true
    });
    
    axios.get(URL + '?url=' + nextPage)
      .then((response) => {
        let procedures = response.data.lots;
        let pagination = response.data.pagination;
        this.setState({
          procedures: this.state.procedures.concat(procedures),
          pagination,
          loadingPagination: false
        })
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  
}