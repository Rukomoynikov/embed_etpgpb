import Main from 'components/Main.jsx';
import Procedure from 'components/Procedure.jsx'
import { render, h } from 'preact';

window.ETPGPB = {};

window.ETPGPB.renderProcedures = function (options) {
  const container = document.querySelector(options.selector);

  const mode = selectMode(options);
  const theme = selectTheme(options);
  const { url } = options;

  render(<Main mode={mode} theme={theme} url={url} />, container);
};

window.ETPGPB.renderProcedure = function (options) {
  const container = document.querySelector(options.selector);

  render(<Procedure />, container);
};


function selectMode (options) {
  const container = document.querySelector(options.selector);
  const width = container.clientWidth;
  let mode = 'wide';

  if (options.mode === 'slim' || width < 1000) {
    mode = 'slim'
  }

  return mode
}

function selectTheme (options) {
  let theme = options.theme || 'light';

  return theme
}

// http://51.15.42.188/api/procedures?search="спорт"&inn="5050073540"
